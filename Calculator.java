package kg.calculator;

public class Calculator {
	
	public static void useCalculator(double firstNumber, double secondNumber, char operator) {
		
		double result = 0;
		double temp = 0;
		boolean thereIsNoOperator = false;
		
		switch(operator) {
		case '+':
			temp = firstNumber + secondNumber;
			break;
		case '-':
			temp = firstNumber - secondNumber;
			break;
		case '*':
			temp = firstNumber * secondNumber;
			break;
		case '/':
			temp = firstNumber / secondNumber;
			break;
		case '%':
			temp = firstNumber % secondNumber;
			break;
		default:
			thereIsNoOperator = true;
		}
		
		if(!thereIsNoOperator) {
			result = temp;
			System.out.println(result);
		}
		
		
		
	}

}
