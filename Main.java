package kg.calculator;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		Scanner userScanner = new Scanner(System.in);
		System.out.println("Enter your first number!");
		int firstNumber = userScanner.nextInt();
		System.out.println("Enter your second Number!");
		int secondNumber = userScanner.nextInt();
		System.out.println("Enter an operator!");
		char operator = userScanner.next().charAt(0);
		userScanner.nextLine();
		
		Calculator.useCalculator(firstNumber, secondNumber, operator);
		
		userScanner.close();

	}

}
